//loads modules
var express = require('express');
var cp = require('child_process');
var fs = require('fs'); 

//creates application
//ints port
//starts the other request handler script
var app = express();
var port = 1234;
var reqHan = cp.fork('reqHan.js');

//app conf
app.use(express.limit('1mb'));
app.use(express.bodyParser());
app.use(express.methodOverride());



//basic get req handler
// sends basic html page back to the client
app.get('/', function(req, res){
fs.readFile('page.html', function(e, d){
	if(e){
		console.log(e);
	}else{
		var page = d;
		console.log(page.toString());
		res.send(page.toString());
	}
});
});

//put req handler
//with child process inwoked for faster execution 
app.put('/', function(req, res, data){
	//starts child which waits for a message
	var childjs=cp.fork('child.js');
	console.log('---------------------------------------------------------------------------------------------------------------');
	req.on('data', function (d){
		var dObj = d.toString();
		console.log(dObj+" from expressServer");
		dObj = JSON.parse(dObj);
		//sends message
		childjs.send({mes: dObj});
		
	});
	//eventlistener for the message back from the child
	childjs.on('message', function(d){
		//console.log(d);
		res.send({text : "answer :"+ d.mes, serverPort : port});
		//kills child
		childjs.kill();
	});
});
//app listens to port
app.listen(port);

