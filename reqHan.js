//loads modules
//creates app
var express = require('express');
var app = express();
var cp = require('child_process');
var port = 8080;

//app conf
app.use(express.limit('1mb'));
app.use(express.bodyParser());
app.use(express.methodOverride());


//basic get req handler
app.get('/', function(req, res){
	res.send("");
});

//put req handler
app.put('/', function(req, res, data){
	console.log('---------------------------------------------------------------------------------------------------------------');
	//needed for cross side XMLHttpRequest
	// res.header("Access-Control-Allow-Origin", "*");
	//to allow from all sides
	res.header("Access-Control-Allow-Origin", "http://188.109.205.9:1234"/*'*'*/);
	req.on('data', function (d){
		var dObj = d.toString();
		console.log("----------"+dObj+" from req handler");
		console.log('---------------------------------------------------------------------------------------------------------------');
		dObj = JSON.parse(dObj);
		// some answer
		res.send("test");
		
	});
});

//needed for Request option method to allow some stuff
app.all('/', function(req, res) {
	res.header("Access-Control-Allow-Origin", "*");
  	res.header("Access-Control-Allow-Headers", "origin, content-type");
  	res.header('Access-Control-Allow-Methods', 'PUT');
  	res.send("test2");
});

//listens to port
app.listen(port);